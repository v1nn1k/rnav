import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { StackNavigator, TabNavigator } from 'react-navigation';

class HomeScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerTintColor: '#5e626e',
    header: null
  });
  go = () => {
    this.props.navigation.navigate('Settings');
  };
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Home Screen</Text>
        <TouchableOpacity onPress={this.go}>
          <Text>Go</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

class AboutScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>About Screen</Text>
      </View>
    );
  }
}

class Settings extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'Settings',
    headerTintColor: '#5e626e'
  });

  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Settings</Text>
      </View>
    );
  }
}
const tabNavigator = TabNavigator(
  {
    Home: {
      screen: HomeScreen
    },
    About: {
      screen: AboutScreen
    }
  },
  {
    tabBarPosition: 'bottom'
  }
);

export default StackNavigator({
  TabNav: {
    screen: tabNavigator
  },
  Settings: {
    screen: Settings,
    headerMode: 'screen'
  }
});
